# `console.speak`

`console.speak('out loud')` & `console.laugh()`

* [Codepen Demo](https://codepen.io/pixelass/pen/PzBWLm)
* [Documentation](https://pixelass.github.io/console-speak/docs/)

## Browser

```html
<script src="//cdn.rawgit.com/pixelass/console-speak/master/dist/index.js"/>
```

## npm

```shell
npm i -D console-speak
```

```js
import consoleSpeak from 'console-speak'
consoleSpeak(window)
```

## Usage

```js

console.speak('Bonjour le monde', {lang: 'fr-FR')

console.speak('Ciao mondo', {lang: 'it-IT'})

console.speak('Hola Mundo', {lang: 'es-ES'})

console.speak('Hallo Welt', {lang: 'de-DE'})

console.speak('Hello World', {rate: .5, pitch: 2})

console.speak('This is hysterical', { voice: 'Hysterical'})

```

## Voices

Lang 'en-US' (default) has some voices to choose from

* Alex
* Agnes
* Albert
* Bad News
* Bahh
* Bells
* Boing
* Bruce
* Bubbles
* Cellos
* Deranged
* Fred
* Good News
* Hysterical
* Junior
* Kathy
* Pipe Organ
* Princess
* Ralph
* Samantha
* Trinoids
* Vicki
* Victoria
* Whisper
* Zarvox


## More fun with laughter

```js
console.laugh() // => 'Hahahahaha', {voice: 'Hysterical'}
console.laugh('LOL') // => 'LOL', {voice: 'Hysterical'}
```

