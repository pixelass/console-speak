# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.4.0"></a>
# [0.4.0](https://github.com/pixelass/console-speak/compare/v0.3.1...v0.4.0) (2016-07-31)


### Features

* **silence:** added a silence method ([7bdf405](https://github.com/pixelass/console-speak/commit/7bdf405))



<a name="0.3.1"></a>
## [0.3.1](https://github.com/pixelass/console-speak/compare/v0.3.0...v0.3.1) (2016-07-30)


### Bug Fixes

* **firefox:** firefox support ([8374afe](https://github.com/pixelass/console-speak/commit/8374afe))



<a name="0.3.0"></a>
# [0.3.0](https://github.com/pixelass/console-speak/compare/v0.2.2...v0.3.0) (2016-07-29)


### Features

* **laugh:** adde laugh ([334c5a2](https://github.com/pixelass/console-speak/commit/334c5a2))



<a name="0.2.2"></a>
## [0.2.2](https://github.com/pixelass/console-speak/compare/v0.2.1...v0.2.2) (2016-07-29)


### Bug Fixes

* **voice:** fixed undefined keys ([ba0c010](https://github.com/pixelass/console-speak/commit/ba0c010))



<a name="0.2.1"></a>
## [0.2.1](https://github.com/pixelass/console-speak/compare/v0.2.0...v0.2.1) (2016-07-29)


### Bug Fixes

* **voice:** add default voice ([51d2864](https://github.com/pixelass/console-speak/commit/51d2864))



<a name="0.2.0"></a>
# [0.2.0](https://github.com/pixelass/console-speak/compare/v0.1.3...v0.2.0) (2016-07-29)


### Features

* **voices:** added voice support ([a21d4b1](https://github.com/pixelass/console-speak/commit/a21d4b1))



<a name="0.1.3"></a>
## [0.1.3](https://github.com/pixelass/console-speak/compare/v0.1.2...v0.1.3) (2016-07-29)



<a name="0.1.2"></a>
## [0.1.2](https://github.com/pixelass/console-speak/compare/v0.1.1...v0.1.2) (2016-07-29)



<a name="0.1.1"></a>
## [0.1.1](https://github.com/pixelass/console-speak/compare/v0.1.0...v0.1.1) (2016-07-29)


### Bug Fixes

* **main:** define correct main file ([127f8b8](https://github.com/pixelass/console-speak/commit/127f8b8))



<a name="0.1.0"></a>
# 0.1.0 (2016-07-29)


### Features

* **alpha version:** first alpha test 66b1527
