/**
 * use `consoleSpeak` on window. When this file is
 * loaded `console.speak` is initialized on window.
 * No further setup is needed. The main purpose of
 * this file is usage in the browser per cdn or similar
 * 
 * @file browser
 * @author Gregor Adams <greg@pixelass.com>
 */
import consoleSpeak from '.'
consoleSpeak(window)