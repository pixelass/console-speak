/**
 * attach `speak` to window and add a shortcut for `laugh`
 * When this file is loaded, `consoleSpeak` needs to be called with `window`
 *
 * @file console
 * @module console
 * @author Gregor Adams <greg@pixelass.com>
 */
import Speak from './speak'

/**
 * add speak to console or throw an Error
 * @param  {Object} GLOBAL - window or other GLOBAL object that supports the API
 */
const consoleSpeak = (GLOBAL)=> {
  try {
   const reader = new Speak(GLOBAL)
   GLOBAL.console.speak = reader.speak
   GLOBAL.console.silence = reader.silence
   GLOBAL.console.laugh = (message='Hahahaha') => reader.speak(message, {voice: 'Hysterical'});
  } catch(err) {
    throw err
  }
}

export default consoleSpeak