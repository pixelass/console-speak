/**
 * text to speech queue handler that uses HTML5 speechSynthesis
 * Voices (en-US):
 *  "Agnes",
 *  "Albert",
 *  "Alex",
 *  "Bad News",
 *  "Bahh","Bells",
 *  "Boing",
 *  "Bruce",
 *  "Bubbles",
 *  "Cellos",
 *  "Deranged",
 *  "Fred",
 *  "Good News",
 *  "Hysterical",
 *  "Junior",
 *  "Kathy",
 *  "Pipe Organ",
 *  "Princess",
 *  "Ralph",
 *  "Samantha",
 *  "Trinoids",
 *  "Vicki",
 *  "Victoria",
 *  "Whisper",
 *  "Zarvox"
 *
 * @file speak
 * @module speak
 * @author Gregor Adams <greg@pixelass.com>
 */

/**
 * create a speaker and synth instance
 * @constructor
 * @param  {Object} GLOBAL window
 * @return {Object} returns the `speak` and `silence` method
 */
class Speak {
  constructor(GLOBAL){
    this.speaker = GLOBAL.speechSynthesis
    this.GLOBAL = GLOBAL
    this.voices = {}
    /**
     * a queue of texts to be spoken when the last
     * one has finished
     * @type {Array}
     */
    this.queue = []
    /**
     * return the `speak` and `silence` method
     */
    return {
      speak:this.speak.bind(this),
      silence: ()=> {
        this.queue = []
        this.speaker.cancel()
      }
    }
  }

  /**
   * a promise that returns a sorted object
   * @return {Promise}
   */
  loadVoices(){
    return new Promise((resolve,reject)=>{
      if (this.speaker.getVoices().length) {
        this.updateVoices()
        resolve(this.voices)
      }
      try {
        this.speaker.onvoiceschanged = e => {
          this.updateVoices()
          resolve(this.voices)
        }
      } catch(err) {
        reject(err)
      }
    })
  }

  /**
   * updates the voices and sorts them
   * @return {Object} sorted voices
   */
  updateVoices(){
    const voices = {}
    const raw = this.speaker.getVoices()
    raw.forEach(voice => {
      voices[voice.lang] = voices[voice.lang] || {}
      voices[voice.lang][voice.name] = voice
    })
    this.voices = voices
  }

  /**
   * returns voices
   * @type {Getter}
   * @return {Object} current voices
   */
  get voices(){
    return this._voices
  }

  /**
   * sets voices
   * @type {Setter}
   * @param {Object} voices - current voices
   */
  set voices(voices){
    this._voices = voices
  }

  /**
   * handler to manage the queue when a speech has ended.
   */
  onEnd(){
    // allow new speech
    this.speaking = false
    // check for queue items
    if (this.queue[0]) {
      // items found:
      // play the next item and remove it from the queue
      const next = this.queue[0]
      this.speak(next.message,next.options)
      this.queue.splice(0,1)
    }
  }

  /**
   * method that actually speaks.
   * This method checks if speaker is already speaking.
   * If `true` the new message and options will be added to
   * the queue, otherwise the message is spoken
   * @param  {String} message - the text that will be synthesised when the
   *                            utterance is spoken.
   * @param  {Object} options - options to use for the utterance.
   * @param  {Number} options.pitch - the pitch at which the utterance will
   *                                  be spoken at.
   * @param  {Number} options.rate - the speed at which the utterance will
   *                                 be spoken at.
   * @param  {Number} options.volume - the volume that the utterance will
   *                                   be spoken at.
   * @param  {String} options.lang - the language of the utterance.
   * @param  {Object} options.voice - the voice that will be used to speak 
   *                                  the utterance.
   */
  speak(message = '', options = {}) {
    if (this.speaking) {
      this.queue.push({message,options})
    } else {
      this.speaking = true
      this.loadVoices().then(voices =>{
        // define some safe options to make make sure only
        // [lang,pitch,rate,volume, voice] can be changed and have defaults
        const {lang,voice} = options
        const getVoice = (lang = 'en-US') => {
          if(lang in voices && voice in voices[lang]) {
            return voices[lang][voice]
          } else if (lang && lang in voices){
            const allVoices = Object.keys(voices[lang])
            const defaultVoice = allVoices[0]
            return voices[lang][defaultVoice]
          } else if ('en-US' in voices && 'Alex' in voices['en-US']) {
            return voices['en-US']['Alex']
          } else {
            return
          }
        }
        const safeOptions = {
          lang: lang || 'en-US',
          pitch: options.pitch || 1,
          rate: options.rate || 1,
          volume: options.volume || 1,
          voice: getVoice(lang),
          text: message
        }
        // block next messages (force to queue)
        // apply options and speak
        // create a new instance (required in firefox)
        this.synth = new this.GLOBAL.SpeechSynthesisUtterance()
        /**
         * assign the `onEnd` method to the synths
         * `onend` listener
         * @type {Function}
         */
        this.synth.onend = this.onEnd.bind(this)
        Object.assign(this.synth, safeOptions)
        this.speaker.speak(this.synth)
      }).catch((err) => { throw err})
    }
  }
}

export default Speak