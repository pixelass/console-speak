/**
 * Default file for npm.
 * When this file is loaded, `consoleSpeak` needs to be called with `window`
 *
 * @file console-speak
 * @module console-speak
 * @author Gregor Adams <greg@pixelass.com>
 */
import consoleSpeak from './console'
export default consoleSpeak
