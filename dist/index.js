(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var _ = require('.');

var _2 = _interopRequireDefault(_);

function _interopRequireDefault(obj) {
                          return obj && obj.__esModule ? obj : { default: obj };
}

(0, _2.default)(window); /**
                          * use `consoleSpeak` on window. When this file is
                          * loaded `console.speak` is initialized on window.
                          * No further setup is needed. The main purpose of
                          * this file is usage in the browser per cdn or similar
                          * 
                          * @file browser
                          * @author Gregor Adams <greg@pixelass.com>
                          */

},{".":3}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _speak = require('./speak');

var _speak2 = _interopRequireDefault(_speak);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * add speak to console or throw an Error
 * @param  {Object} GLOBAL - window or other GLOBAL object that supports the API
 */
var consoleSpeak = function consoleSpeak(GLOBAL) {
  try {
    (function () {
      var reader = new _speak2.default(GLOBAL);
      GLOBAL.console.speak = reader.speak;
      GLOBAL.console.silence = reader.silence;
      GLOBAL.console.laugh = function () {
        var message = arguments.length <= 0 || arguments[0] === undefined ? 'Hahahaha' : arguments[0];
        return reader.speak(message, { voice: 'Hysterical' });
      };
    })();
  } catch (err) {
    throw err;
  }
}; /**
    * attach `speak` to window and add a shortcut for `laugh`
    * When this file is loaded, `consoleSpeak` needs to be called with `window`
    *
    * @file console
    * @module console
    * @author Gregor Adams <greg@pixelass.com>
    */
exports.default = consoleSpeak;

},{"./speak":4}],3:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _console = require('./console');

var _console2 = _interopRequireDefault(_console);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

exports.default = _console2.default; /**
                                      * Default file for npm.
                                      * When this file is loaded, `consoleSpeak` needs to be called with `window`
                                      *
                                      * @file console-speak
                                      * @module console-speak
                                      * @author Gregor Adams <greg@pixelass.com>
                                      */

},{"./console":2}],4:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}();

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

/**
 * text to speech queue handler that uses HTML5 speechSynthesis
 * Voices (en-US):
 *  "Agnes",
 *  "Albert",
 *  "Alex",
 *  "Bad News",
 *  "Bahh","Bells",
 *  "Boing",
 *  "Bruce",
 *  "Bubbles",
 *  "Cellos",
 *  "Deranged",
 *  "Fred",
 *  "Good News",
 *  "Hysterical",
 *  "Junior",
 *  "Kathy",
 *  "Pipe Organ",
 *  "Princess",
 *  "Ralph",
 *  "Samantha",
 *  "Trinoids",
 *  "Vicki",
 *  "Victoria",
 *  "Whisper",
 *  "Zarvox"
 *
 * @file speak
 * @module speak
 * @author Gregor Adams <greg@pixelass.com>
 */

/**
 * create a speaker and synth instance
 * @constructor
 * @param  {Object} GLOBAL window
 * @return {Object} returns the `speak` and `silence` method
 */
var Speak = function () {
  function Speak(GLOBAL) {
    var _this = this;

    _classCallCheck(this, Speak);

    this.speaker = GLOBAL.speechSynthesis;
    this.GLOBAL = GLOBAL;
    this.voices = {};
    /**
     * a queue of texts to be spoken when the last
     * one has finished
     * @type {Array}
     */
    this.queue = [];
    /**
     * return the `speak` and `silence` method
     */
    return {
      speak: this.speak.bind(this),
      silence: function silence() {
        _this.queue = [];
        _this.speaker.cancel();
      }
    };
  }

  /**
   * a promise that returns a sorted object
   * @return {Promise}
   */

  _createClass(Speak, [{
    key: 'loadVoices',
    value: function loadVoices() {
      var _this2 = this;

      return new Promise(function (resolve, reject) {
        if (_this2.speaker.getVoices().length) {
          _this2.updateVoices();
          resolve(_this2.voices);
        }
        try {
          _this2.speaker.onvoiceschanged = function (e) {
            _this2.updateVoices();
            resolve(_this2.voices);
          };
        } catch (err) {
          reject(err);
        }
      });
    }

    /**
     * updates the voices and sorts them
     * @return {Object} sorted voices
     */

  }, {
    key: 'updateVoices',
    value: function updateVoices() {
      var voices = {};
      var raw = this.speaker.getVoices();
      raw.forEach(function (voice) {
        voices[voice.lang] = voices[voice.lang] || {};
        voices[voice.lang][voice.name] = voice;
      });
      this.voices = voices;
    }

    /**
     * returns voices
     * @type {Getter}
     * @return {Object} current voices
     */

  }, {
    key: 'onEnd',

    /**
     * handler to manage the queue when a speech has ended.
     */
    value: function onEnd() {
      // allow new speech
      this.speaking = false;
      // check for queue items
      if (this.queue[0]) {
        // items found:
        // play the next item and remove it from the queue
        var next = this.queue[0];
        this.speak(next.message, next.options);
        this.queue.splice(0, 1);
      }
    }

    /**
     * method that actually speaks.
     * This method checks if speaker is already speaking.
     * If `true` the new message and options will be added to
     * the queue, otherwise the message is spoken
     * @param  {String} message - the text that will be synthesised when the
     *                            utterance is spoken.
     * @param  {Object} options - options to use for the utterance.
     * @param  {Number} options.pitch - the pitch at which the utterance will
     *                                  be spoken at.
     * @param  {Number} options.rate - the speed at which the utterance will
     *                                 be spoken at.
     * @param  {Number} options.volume - the volume that the utterance will
     *                                   be spoken at.
     * @param  {String} options.lang - the language of the utterance.
     * @param  {Object} options.voice - the voice that will be used to speak 
     *                                  the utterance.
     */

  }, {
    key: 'speak',
    value: function speak() {
      var _this3 = this;

      var message = arguments.length <= 0 || arguments[0] === undefined ? '' : arguments[0];
      var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

      if (this.speaking) {
        this.queue.push({ message: message, options: options });
      } else {
        this.speaking = true;
        this.loadVoices().then(function (voices) {
          // define some safe options to make make sure only
          // [lang,pitch,rate,volume, voice] can be changed and have defaults
          var lang = options.lang;
          var voice = options.voice;

          var getVoice = function getVoice() {
            var lang = arguments.length <= 0 || arguments[0] === undefined ? 'en-US' : arguments[0];

            if (lang in voices && voice in voices[lang]) {
              return voices[lang][voice];
            } else if (lang && lang in voices) {
              var allVoices = Object.keys(voices[lang]);
              var defaultVoice = allVoices[0];
              return voices[lang][defaultVoice];
            } else if ('en-US' in voices && 'Alex' in voices['en-US']) {
              return voices['en-US']['Alex'];
            } else {
              return;
            }
          };
          var safeOptions = {
            lang: lang || 'en-US',
            pitch: options.pitch || 1,
            rate: options.rate || 1,
            volume: options.volume || 1,
            voice: getVoice(lang),
            text: message
          };
          // block next messages (force to queue)
          // apply options and speak
          // create a new instance (required in firefox)
          _this3.synth = new _this3.GLOBAL.SpeechSynthesisUtterance();
          /**
           * assign the `onEnd` method to the synths
           * `onend` listener
           * @type {Function}
           */
          _this3.synth.onend = _this3.onEnd.bind(_this3);
          Object.assign(_this3.synth, safeOptions);
          _this3.speaker.speak(_this3.synth);
        }).catch(function (err) {
          throw err;
        });
      }
    }
  }, {
    key: 'voices',
    get: function get() {
      return this._voices;
    }

    /**
     * sets voices
     * @type {Setter}
     * @param {Object} voices - current voices
     */

    , set: function set(voices) {
      this._voices = voices;
    }
  }]);

  return Speak;
}();

exports.default = Speak;

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvYnJvd3Nlci5qcyIsInNyYy9jb25zb2xlLmpzIiwic3JjL2luZGV4LmpzIiwic3JjL3NwZWFrLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7QUNTQTs7Ozs7Ozs7QUFDQSxnQixBQUFBLEFBQWEsU0FWYjs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNRQTs7Ozs7Ozs7QUFFQTs7OztBQUlBLElBQU0sZUFBZSxTQUFmLEFBQWUsYUFBQSxBQUFDLFFBQVUsQUFDOUI7TUFBSTtpQkFDSDtVQUFNLFNBQVMsb0JBQWYsQUFBZSxBQUFVLEFBQ3pCO2FBQUEsQUFBTyxRQUFQLEFBQWUsUUFBUSxPQUF2QixBQUE4QixBQUM5QjthQUFBLEFBQU8sUUFBUCxBQUFlLFVBQVUsT0FBekIsQUFBZ0MsQUFDaEM7YUFBQSxBQUFPLFFBQVAsQUFBZSxRQUFRLFlBQUE7WUFBQSxBQUFDLGdFQUFELEFBQVMsdUJBQVQ7ZUFBd0IsT0FBQSxBQUFPLE1BQVAsQUFBYSxTQUFTLEVBQUMsT0FBL0MsQUFBd0IsQUFBc0IsQUFBUTtBQUoxRSxBQUlIO0FBQ0E7QUFMRCxJQUtFLE9BQUEsQUFBTSxLQUFLLEFBQ1g7VUFBQSxBQUFNLEFBQ1A7QUFDRjtBLEFBVEQsR0FkQTs7Ozs7Ozs7a0IsQUF5QmU7Ozs7Ozs7OztBQ2pCZjs7Ozs7Ozs7cUNBUkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlDQTs7Ozs7O0ksQUFNTSxvQkFDSjtpQkFBQSxBQUFZLFFBQU87Z0JBQUE7OzBCQUNqQjs7U0FBQSxBQUFLLFVBQVUsT0FBZixBQUFzQixBQUN0QjtTQUFBLEFBQUssU0FBTCxBQUFjLEFBQ2Q7U0FBQSxBQUFLLFNBQUwsQUFBYyxBQUNkO0FBS0E7Ozs7O1NBQUEsQUFBSyxRQUFMLEFBQWEsQUFDYjtBQUdBOzs7O2FBQ1EsS0FBQSxBQUFLLE1BQUwsQUFBVyxLQURaLEFBQ0MsQUFBZ0IsQUFDdEI7ZUFBUyxtQkFBSyxBQUNaO2NBQUEsQUFBSyxRQUFMLEFBQWEsQUFDYjtjQUFBLEFBQUssUUFBTCxBQUFhLEFBQ2Q7QUFMSCxBQUFPLEFBT1I7QUFQUSxBQUNMO0FBUUo7Ozs7Ozs7OztpQ0FJWTttQkFDVjs7aUJBQU8sQUFBSSxRQUFRLFVBQUEsQUFBQyxTQUFELEFBQVMsUUFBUyxBQUNuQztZQUFJLE9BQUEsQUFBSyxRQUFMLEFBQWEsWUFBakIsQUFBNkIsUUFBUSxBQUNuQztpQkFBQSxBQUFLLEFBQ0w7a0JBQVEsT0FBUixBQUFhLEFBQ2Q7QUFDRDtZQUFJLEFBQ0Y7aUJBQUEsQUFBSyxRQUFMLEFBQWEsa0JBQWtCLGFBQUssQUFDbEM7bUJBQUEsQUFBSyxBQUNMO29CQUFRLE9BQVIsQUFBYSxBQUNkO0FBSEQsQUFJRDtBQUxELFVBS0UsT0FBQSxBQUFNLEtBQUssQUFDWDtpQkFBQSxBQUFPLEFBQ1I7QUFDRjtBQWJELEFBQU8sQUFjUixPQWRRO0FBZ0JUOzs7Ozs7Ozs7bUNBSWMsQUFDWjtVQUFNLFNBQU4sQUFBZSxBQUNmO1VBQU0sTUFBTSxLQUFBLEFBQUssUUFBakIsQUFBWSxBQUFhLEFBQ3pCO1VBQUEsQUFBSSxRQUFRLGlCQUFTLEFBQ25CO2VBQU8sTUFBUCxBQUFhLFFBQVEsT0FBTyxNQUFQLEFBQWEsU0FBbEMsQUFBMkMsQUFDM0M7ZUFBTyxNQUFQLEFBQWEsTUFBTSxNQUFuQixBQUF5QixRQUF6QixBQUFpQyxBQUNsQztBQUhELEFBSUE7V0FBQSxBQUFLLFNBQUwsQUFBYyxBQUNmO0FBRUQ7Ozs7Ozs7OztTQWtCQTs7Ozs7NEJBR08sQUFDTDtBQUNBO1dBQUEsQUFBSyxXQUFMLEFBQWdCLEFBQ2hCO0FBQ0E7VUFBSSxLQUFBLEFBQUssTUFBVCxBQUFJLEFBQVcsSUFBSSxBQUNqQjtBQUNBO0FBQ0E7WUFBTSxPQUFPLEtBQUEsQUFBSyxNQUFsQixBQUFhLEFBQVcsQUFDeEI7YUFBQSxBQUFLLE1BQU0sS0FBWCxBQUFnQixTQUFRLEtBQXhCLEFBQTZCLEFBQzdCO2FBQUEsQUFBSyxNQUFMLEFBQVcsT0FBWCxBQUFrQixHQUFsQixBQUFvQixBQUNyQjtBQUNGO0FBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzRCQWtCa0M7bUJBQUE7O1VBQTVCLEFBQTRCLGdFQUFsQixBQUFrQixlQUFBO1VBQWQsQUFBYyxnRUFBSixBQUFJLGVBQ2hDOztVQUFJLEtBQUosQUFBUyxVQUFVLEFBQ2pCO2FBQUEsQUFBSyxNQUFMLEFBQVcsS0FBSyxFQUFDLFNBQUQsU0FBUyxTQUF6QixBQUFnQixBQUNqQjtBQUZELGFBRU8sQUFDTDthQUFBLEFBQUssV0FBTCxBQUFnQixBQUNoQjthQUFBLEFBQUssYUFBTCxBQUFrQixLQUFLLGtCQUFTLEFBQzlCO0FBQ0E7QUFGOEI7Y0FBQSxBQUd2QixPQUh1QixBQUdULFFBSFMsQUFHdkI7Y0FIdUIsQUFHbEIsUUFIa0IsQUFHVCxRQUhTLEFBR2xCLEFBQ1o7O2NBQU0sV0FBVyxTQUFYLEFBQVcsV0FBb0I7Z0JBQW5CLEFBQW1CLDZEQUFaLEFBQVksb0JBQ25DOztnQkFBRyxRQUFBLEFBQVEsVUFBVSxTQUFTLE9BQTlCLEFBQThCLEFBQU8sT0FBTyxBQUMxQztxQkFBTyxPQUFBLEFBQU8sTUFBZCxBQUFPLEFBQWEsQUFDckI7QUFGRCx1QkFFVyxRQUFRLFFBQVosQUFBb0IsUUFBTyxBQUNoQztrQkFBTSxZQUFZLE9BQUEsQUFBTyxLQUFLLE9BQTlCLEFBQWtCLEFBQVksQUFBTyxBQUNyQztrQkFBTSxlQUFlLFVBQXJCLEFBQXFCLEFBQVUsQUFDL0I7cUJBQU8sT0FBQSxBQUFPLE1BQWQsQUFBTyxBQUFhLEFBQ3JCO0FBSk0sYUFBQSxVQUlJLFdBQUEsQUFBVyxVQUFVLFVBQVUsT0FBbkMsQUFBbUMsQUFBTyxVQUFVLEFBQ3pEO3FCQUFPLE9BQUEsQUFBTyxTQUFkLEFBQU8sQUFBZ0IsQUFDeEI7QUFGTSxhQUFBLE1BRUEsQUFDTDtBQUNEO0FBQ0Y7QUFaRCxBQWFBO2NBQU07a0JBQ0UsUUFEWSxBQUNKLEFBQ2Q7bUJBQU8sUUFBQSxBQUFRLFNBRkcsQUFFTSxBQUN4QjtrQkFBTSxRQUFBLEFBQVEsUUFISSxBQUdJLEFBQ3RCO29CQUFRLFFBQUEsQUFBUSxVQUpFLEFBSVEsQUFDMUI7bUJBQU8sU0FMVyxBQUtYLEFBQVMsQUFDaEI7a0JBTkYsQUFBb0IsQUFNWixBQUVSO0FBUm9CLEFBQ2xCO0FBUUY7QUFDQTtBQUNBO2lCQUFBLEFBQUssUUFBUSxJQUFJLE9BQUEsQUFBSyxPQUF0QixBQUFhLEFBQWdCLEFBQzdCO0FBS0E7Ozs7O2lCQUFBLEFBQUssTUFBTCxBQUFXLFFBQVEsT0FBQSxBQUFLLE1BQUwsQUFBVyxLQUE5QixBQUNBO2lCQUFBLEFBQU8sT0FBTyxPQUFkLEFBQW1CLE9BQW5CLEFBQTBCLEFBQzFCO2lCQUFBLEFBQUssUUFBTCxBQUFhLE1BQU0sT0FBbkIsQUFBd0IsQUFDekI7QUFyQ0QsV0FBQSxBQXFDRyxNQUFNLFVBQUEsQUFBQyxLQUFRLEFBQUU7Z0JBQUEsQUFBTSxBQUFJO0FBckM5QixBQXNDRDtBQUNGOzs7O3dCQTNGVyxBQUNWO2FBQU8sS0FBUCxBQUFZLEFBQ2I7QUFFRDs7Ozs7Ozs7d0IsQUFLVyxRQUFPLEFBQ2hCO1dBQUEsQUFBSyxVQUFMLEFBQWUsQUFDaEI7Ozs7Ozs7a0IsQUFtRlkiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiLyoqXG4gKiB1c2UgYGNvbnNvbGVTcGVha2Agb24gd2luZG93LiBXaGVuIHRoaXMgZmlsZSBpc1xuICogbG9hZGVkIGBjb25zb2xlLnNwZWFrYCBpcyBpbml0aWFsaXplZCBvbiB3aW5kb3cuXG4gKiBObyBmdXJ0aGVyIHNldHVwIGlzIG5lZWRlZC4gVGhlIG1haW4gcHVycG9zZSBvZlxuICogdGhpcyBmaWxlIGlzIHVzYWdlIGluIHRoZSBicm93c2VyIHBlciBjZG4gb3Igc2ltaWxhclxuICogXG4gKiBAZmlsZSBicm93c2VyXG4gKiBAYXV0aG9yIEdyZWdvciBBZGFtcyA8Z3JlZ0BwaXhlbGFzcy5jb20+XG4gKi9cbmltcG9ydCBjb25zb2xlU3BlYWsgZnJvbSAnLidcbmNvbnNvbGVTcGVhayh3aW5kb3cpIiwiLyoqXG4gKiBhdHRhY2ggYHNwZWFrYCB0byB3aW5kb3cgYW5kIGFkZCBhIHNob3J0Y3V0IGZvciBgbGF1Z2hgXG4gKiBXaGVuIHRoaXMgZmlsZSBpcyBsb2FkZWQsIGBjb25zb2xlU3BlYWtgIG5lZWRzIHRvIGJlIGNhbGxlZCB3aXRoIGB3aW5kb3dgXG4gKlxuICogQGZpbGUgY29uc29sZVxuICogQG1vZHVsZSBjb25zb2xlXG4gKiBAYXV0aG9yIEdyZWdvciBBZGFtcyA8Z3JlZ0BwaXhlbGFzcy5jb20+XG4gKi9cbmltcG9ydCBTcGVhayBmcm9tICcuL3NwZWFrJ1xuXG4vKipcbiAqIGFkZCBzcGVhayB0byBjb25zb2xlIG9yIHRocm93IGFuIEVycm9yXG4gKiBAcGFyYW0gIHtPYmplY3R9IEdMT0JBTCAtIHdpbmRvdyBvciBvdGhlciBHTE9CQUwgb2JqZWN0IHRoYXQgc3VwcG9ydHMgdGhlIEFQSVxuICovXG5jb25zdCBjb25zb2xlU3BlYWsgPSAoR0xPQkFMKT0+IHtcbiAgdHJ5IHtcbiAgIGNvbnN0IHJlYWRlciA9IG5ldyBTcGVhayhHTE9CQUwpXG4gICBHTE9CQUwuY29uc29sZS5zcGVhayA9IHJlYWRlci5zcGVha1xuICAgR0xPQkFMLmNvbnNvbGUuc2lsZW5jZSA9IHJlYWRlci5zaWxlbmNlXG4gICBHTE9CQUwuY29uc29sZS5sYXVnaCA9IChtZXNzYWdlPSdIYWhhaGFoYScpID0+IHJlYWRlci5zcGVhayhtZXNzYWdlLCB7dm9pY2U6ICdIeXN0ZXJpY2FsJ30pO1xuICB9IGNhdGNoKGVycikge1xuICAgIHRocm93IGVyclxuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IGNvbnNvbGVTcGVhayIsIi8qKlxuICogRGVmYXVsdCBmaWxlIGZvciBucG0uXG4gKiBXaGVuIHRoaXMgZmlsZSBpcyBsb2FkZWQsIGBjb25zb2xlU3BlYWtgIG5lZWRzIHRvIGJlIGNhbGxlZCB3aXRoIGB3aW5kb3dgXG4gKlxuICogQGZpbGUgY29uc29sZS1zcGVha1xuICogQG1vZHVsZSBjb25zb2xlLXNwZWFrXG4gKiBAYXV0aG9yIEdyZWdvciBBZGFtcyA8Z3JlZ0BwaXhlbGFzcy5jb20+XG4gKi9cbmltcG9ydCBjb25zb2xlU3BlYWsgZnJvbSAnLi9jb25zb2xlJ1xuZXhwb3J0IGRlZmF1bHQgY29uc29sZVNwZWFrXG4iLCIvKipcbiAqIHRleHQgdG8gc3BlZWNoIHF1ZXVlIGhhbmRsZXIgdGhhdCB1c2VzIEhUTUw1IHNwZWVjaFN5bnRoZXNpc1xuICogVm9pY2VzIChlbi1VUyk6XG4gKiAgXCJBZ25lc1wiLFxuICogIFwiQWxiZXJ0XCIsXG4gKiAgXCJBbGV4XCIsXG4gKiAgXCJCYWQgTmV3c1wiLFxuICogIFwiQmFoaFwiLFwiQmVsbHNcIixcbiAqICBcIkJvaW5nXCIsXG4gKiAgXCJCcnVjZVwiLFxuICogIFwiQnViYmxlc1wiLFxuICogIFwiQ2VsbG9zXCIsXG4gKiAgXCJEZXJhbmdlZFwiLFxuICogIFwiRnJlZFwiLFxuICogIFwiR29vZCBOZXdzXCIsXG4gKiAgXCJIeXN0ZXJpY2FsXCIsXG4gKiAgXCJKdW5pb3JcIixcbiAqICBcIkthdGh5XCIsXG4gKiAgXCJQaXBlIE9yZ2FuXCIsXG4gKiAgXCJQcmluY2Vzc1wiLFxuICogIFwiUmFscGhcIixcbiAqICBcIlNhbWFudGhhXCIsXG4gKiAgXCJUcmlub2lkc1wiLFxuICogIFwiVmlja2lcIixcbiAqICBcIlZpY3RvcmlhXCIsXG4gKiAgXCJXaGlzcGVyXCIsXG4gKiAgXCJaYXJ2b3hcIlxuICpcbiAqIEBmaWxlIHNwZWFrXG4gKiBAbW9kdWxlIHNwZWFrXG4gKiBAYXV0aG9yIEdyZWdvciBBZGFtcyA8Z3JlZ0BwaXhlbGFzcy5jb20+XG4gKi9cblxuLyoqXG4gKiBjcmVhdGUgYSBzcGVha2VyIGFuZCBzeW50aCBpbnN0YW5jZVxuICogQGNvbnN0cnVjdG9yXG4gKiBAcGFyYW0gIHtPYmplY3R9IEdMT0JBTCB3aW5kb3dcbiAqIEByZXR1cm4ge09iamVjdH0gcmV0dXJucyB0aGUgYHNwZWFrYCBhbmQgYHNpbGVuY2VgIG1ldGhvZFxuICovXG5jbGFzcyBTcGVhayB7XG4gIGNvbnN0cnVjdG9yKEdMT0JBTCl7XG4gICAgdGhpcy5zcGVha2VyID0gR0xPQkFMLnNwZWVjaFN5bnRoZXNpc1xuICAgIHRoaXMuR0xPQkFMID0gR0xPQkFMXG4gICAgdGhpcy52b2ljZXMgPSB7fVxuICAgIC8qKlxuICAgICAqIGEgcXVldWUgb2YgdGV4dHMgdG8gYmUgc3Bva2VuIHdoZW4gdGhlIGxhc3RcbiAgICAgKiBvbmUgaGFzIGZpbmlzaGVkXG4gICAgICogQHR5cGUge0FycmF5fVxuICAgICAqL1xuICAgIHRoaXMucXVldWUgPSBbXVxuICAgIC8qKlxuICAgICAqIHJldHVybiB0aGUgYHNwZWFrYCBhbmQgYHNpbGVuY2VgIG1ldGhvZFxuICAgICAqL1xuICAgIHJldHVybiB7XG4gICAgICBzcGVhazp0aGlzLnNwZWFrLmJpbmQodGhpcyksXG4gICAgICBzaWxlbmNlOiAoKT0+IHtcbiAgICAgICAgdGhpcy5xdWV1ZSA9IFtdXG4gICAgICAgIHRoaXMuc3BlYWtlci5jYW5jZWwoKVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBhIHByb21pc2UgdGhhdCByZXR1cm5zIGEgc29ydGVkIG9iamVjdFxuICAgKiBAcmV0dXJuIHtQcm9taXNlfVxuICAgKi9cbiAgbG9hZFZvaWNlcygpe1xuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSxyZWplY3QpPT57XG4gICAgICBpZiAodGhpcy5zcGVha2VyLmdldFZvaWNlcygpLmxlbmd0aCkge1xuICAgICAgICB0aGlzLnVwZGF0ZVZvaWNlcygpXG4gICAgICAgIHJlc29sdmUodGhpcy52b2ljZXMpXG4gICAgICB9XG4gICAgICB0cnkge1xuICAgICAgICB0aGlzLnNwZWFrZXIub252b2ljZXNjaGFuZ2VkID0gZSA9PiB7XG4gICAgICAgICAgdGhpcy51cGRhdGVWb2ljZXMoKVxuICAgICAgICAgIHJlc29sdmUodGhpcy52b2ljZXMpXG4gICAgICAgIH1cbiAgICAgIH0gY2F0Y2goZXJyKSB7XG4gICAgICAgIHJlamVjdChlcnIpXG4gICAgICB9XG4gICAgfSlcbiAgfVxuXG4gIC8qKlxuICAgKiB1cGRhdGVzIHRoZSB2b2ljZXMgYW5kIHNvcnRzIHRoZW1cbiAgICogQHJldHVybiB7T2JqZWN0fSBzb3J0ZWQgdm9pY2VzXG4gICAqL1xuICB1cGRhdGVWb2ljZXMoKXtcbiAgICBjb25zdCB2b2ljZXMgPSB7fVxuICAgIGNvbnN0IHJhdyA9IHRoaXMuc3BlYWtlci5nZXRWb2ljZXMoKVxuICAgIHJhdy5mb3JFYWNoKHZvaWNlID0+IHtcbiAgICAgIHZvaWNlc1t2b2ljZS5sYW5nXSA9IHZvaWNlc1t2b2ljZS5sYW5nXSB8fCB7fVxuICAgICAgdm9pY2VzW3ZvaWNlLmxhbmddW3ZvaWNlLm5hbWVdID0gdm9pY2VcbiAgICB9KVxuICAgIHRoaXMudm9pY2VzID0gdm9pY2VzXG4gIH1cblxuICAvKipcbiAgICogcmV0dXJucyB2b2ljZXNcbiAgICogQHR5cGUge0dldHRlcn1cbiAgICogQHJldHVybiB7T2JqZWN0fSBjdXJyZW50IHZvaWNlc1xuICAgKi9cbiAgZ2V0IHZvaWNlcygpe1xuICAgIHJldHVybiB0aGlzLl92b2ljZXNcbiAgfVxuXG4gIC8qKlxuICAgKiBzZXRzIHZvaWNlc1xuICAgKiBAdHlwZSB7U2V0dGVyfVxuICAgKiBAcGFyYW0ge09iamVjdH0gdm9pY2VzIC0gY3VycmVudCB2b2ljZXNcbiAgICovXG4gIHNldCB2b2ljZXModm9pY2VzKXtcbiAgICB0aGlzLl92b2ljZXMgPSB2b2ljZXNcbiAgfVxuXG4gIC8qKlxuICAgKiBoYW5kbGVyIHRvIG1hbmFnZSB0aGUgcXVldWUgd2hlbiBhIHNwZWVjaCBoYXMgZW5kZWQuXG4gICAqL1xuICBvbkVuZCgpe1xuICAgIC8vIGFsbG93IG5ldyBzcGVlY2hcbiAgICB0aGlzLnNwZWFraW5nID0gZmFsc2VcbiAgICAvLyBjaGVjayBmb3IgcXVldWUgaXRlbXNcbiAgICBpZiAodGhpcy5xdWV1ZVswXSkge1xuICAgICAgLy8gaXRlbXMgZm91bmQ6XG4gICAgICAvLyBwbGF5IHRoZSBuZXh0IGl0ZW0gYW5kIHJlbW92ZSBpdCBmcm9tIHRoZSBxdWV1ZVxuICAgICAgY29uc3QgbmV4dCA9IHRoaXMucXVldWVbMF1cbiAgICAgIHRoaXMuc3BlYWsobmV4dC5tZXNzYWdlLG5leHQub3B0aW9ucylcbiAgICAgIHRoaXMucXVldWUuc3BsaWNlKDAsMSlcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogbWV0aG9kIHRoYXQgYWN0dWFsbHkgc3BlYWtzLlxuICAgKiBUaGlzIG1ldGhvZCBjaGVja3MgaWYgc3BlYWtlciBpcyBhbHJlYWR5IHNwZWFraW5nLlxuICAgKiBJZiBgdHJ1ZWAgdGhlIG5ldyBtZXNzYWdlIGFuZCBvcHRpb25zIHdpbGwgYmUgYWRkZWQgdG9cbiAgICogdGhlIHF1ZXVlLCBvdGhlcndpc2UgdGhlIG1lc3NhZ2UgaXMgc3Bva2VuXG4gICAqIEBwYXJhbSAge1N0cmluZ30gbWVzc2FnZSAtIHRoZSB0ZXh0IHRoYXQgd2lsbCBiZSBzeW50aGVzaXNlZCB3aGVuIHRoZVxuICAgKiAgICAgICAgICAgICAgICAgICAgICAgICAgICB1dHRlcmFuY2UgaXMgc3Bva2VuLlxuICAgKiBAcGFyYW0gIHtPYmplY3R9IG9wdGlvbnMgLSBvcHRpb25zIHRvIHVzZSBmb3IgdGhlIHV0dGVyYW5jZS5cbiAgICogQHBhcmFtICB7TnVtYmVyfSBvcHRpb25zLnBpdGNoIC0gdGhlIHBpdGNoIGF0IHdoaWNoIHRoZSB1dHRlcmFuY2Ugd2lsbFxuICAgKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBiZSBzcG9rZW4gYXQuXG4gICAqIEBwYXJhbSAge051bWJlcn0gb3B0aW9ucy5yYXRlIC0gdGhlIHNwZWVkIGF0IHdoaWNoIHRoZSB1dHRlcmFuY2Ugd2lsbFxuICAgKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJlIHNwb2tlbiBhdC5cbiAgICogQHBhcmFtICB7TnVtYmVyfSBvcHRpb25zLnZvbHVtZSAtIHRoZSB2b2x1bWUgdGhhdCB0aGUgdXR0ZXJhbmNlIHdpbGxcbiAgICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJlIHNwb2tlbiBhdC5cbiAgICogQHBhcmFtICB7U3RyaW5nfSBvcHRpb25zLmxhbmcgLSB0aGUgbGFuZ3VhZ2Ugb2YgdGhlIHV0dGVyYW5jZS5cbiAgICogQHBhcmFtICB7T2JqZWN0fSBvcHRpb25zLnZvaWNlIC0gdGhlIHZvaWNlIHRoYXQgd2lsbCBiZSB1c2VkIHRvIHNwZWFrIFxuICAgKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGUgdXR0ZXJhbmNlLlxuICAgKi9cbiAgc3BlYWsobWVzc2FnZSA9ICcnLCBvcHRpb25zID0ge30pIHtcbiAgICBpZiAodGhpcy5zcGVha2luZykge1xuICAgICAgdGhpcy5xdWV1ZS5wdXNoKHttZXNzYWdlLG9wdGlvbnN9KVxuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNwZWFraW5nID0gdHJ1ZVxuICAgICAgdGhpcy5sb2FkVm9pY2VzKCkudGhlbih2b2ljZXMgPT57XG4gICAgICAgIC8vIGRlZmluZSBzb21lIHNhZmUgb3B0aW9ucyB0byBtYWtlIG1ha2Ugc3VyZSBvbmx5XG4gICAgICAgIC8vIFtsYW5nLHBpdGNoLHJhdGUsdm9sdW1lLCB2b2ljZV0gY2FuIGJlIGNoYW5nZWQgYW5kIGhhdmUgZGVmYXVsdHNcbiAgICAgICAgY29uc3Qge2xhbmcsdm9pY2V9ID0gb3B0aW9uc1xuICAgICAgICBjb25zdCBnZXRWb2ljZSA9IChsYW5nID0gJ2VuLVVTJykgPT4ge1xuICAgICAgICAgIGlmKGxhbmcgaW4gdm9pY2VzICYmIHZvaWNlIGluIHZvaWNlc1tsYW5nXSkge1xuICAgICAgICAgICAgcmV0dXJuIHZvaWNlc1tsYW5nXVt2b2ljZV1cbiAgICAgICAgICB9IGVsc2UgaWYgKGxhbmcgJiYgbGFuZyBpbiB2b2ljZXMpe1xuICAgICAgICAgICAgY29uc3QgYWxsVm9pY2VzID0gT2JqZWN0LmtleXModm9pY2VzW2xhbmddKVxuICAgICAgICAgICAgY29uc3QgZGVmYXVsdFZvaWNlID0gYWxsVm9pY2VzWzBdXG4gICAgICAgICAgICByZXR1cm4gdm9pY2VzW2xhbmddW2RlZmF1bHRWb2ljZV1cbiAgICAgICAgICB9IGVsc2UgaWYgKCdlbi1VUycgaW4gdm9pY2VzICYmICdBbGV4JyBpbiB2b2ljZXNbJ2VuLVVTJ10pIHtcbiAgICAgICAgICAgIHJldHVybiB2b2ljZXNbJ2VuLVVTJ11bJ0FsZXgnXVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgY29uc3Qgc2FmZU9wdGlvbnMgPSB7XG4gICAgICAgICAgbGFuZzogbGFuZyB8fCAnZW4tVVMnLFxuICAgICAgICAgIHBpdGNoOiBvcHRpb25zLnBpdGNoIHx8IDEsXG4gICAgICAgICAgcmF0ZTogb3B0aW9ucy5yYXRlIHx8IDEsXG4gICAgICAgICAgdm9sdW1lOiBvcHRpb25zLnZvbHVtZSB8fCAxLFxuICAgICAgICAgIHZvaWNlOiBnZXRWb2ljZShsYW5nKSxcbiAgICAgICAgICB0ZXh0OiBtZXNzYWdlXG4gICAgICAgIH1cbiAgICAgICAgLy8gYmxvY2sgbmV4dCBtZXNzYWdlcyAoZm9yY2UgdG8gcXVldWUpXG4gICAgICAgIC8vIGFwcGx5IG9wdGlvbnMgYW5kIHNwZWFrXG4gICAgICAgIC8vIGNyZWF0ZSBhIG5ldyBpbnN0YW5jZSAocmVxdWlyZWQgaW4gZmlyZWZveClcbiAgICAgICAgdGhpcy5zeW50aCA9IG5ldyB0aGlzLkdMT0JBTC5TcGVlY2hTeW50aGVzaXNVdHRlcmFuY2UoKVxuICAgICAgICAvKipcbiAgICAgICAgICogYXNzaWduIHRoZSBgb25FbmRgIG1ldGhvZCB0byB0aGUgc3ludGhzXG4gICAgICAgICAqIGBvbmVuZGAgbGlzdGVuZXJcbiAgICAgICAgICogQHR5cGUge0Z1bmN0aW9ufVxuICAgICAgICAgKi9cbiAgICAgICAgdGhpcy5zeW50aC5vbmVuZCA9IHRoaXMub25FbmQuYmluZCh0aGlzKVxuICAgICAgICBPYmplY3QuYXNzaWduKHRoaXMuc3ludGgsIHNhZmVPcHRpb25zKVxuICAgICAgICB0aGlzLnNwZWFrZXIuc3BlYWsodGhpcy5zeW50aClcbiAgICAgIH0pLmNhdGNoKChlcnIpID0+IHsgdGhyb3cgZXJyfSlcbiAgICB9XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgU3BlYWsiXX0=
